# README #

# gotoActionExampleRos 1.0 package 

This package is the implementation of gotoActionExample example of ARIA library in ROS. To refer to documentation of example or this library, go to usr/local/Aria/docs/gotoActionExample_8cpp-example.html. This package were developed and tested under Ubuntu 14.04.

## Dependencies

* ROS Indigo (http://wiki.ros.org/indigo/Installation)
+ Adept Mobilerobots libraries (http://robots.mobilerobots.com/wiki/All_Software):
    * LibAria 2.9.0
    * MobileSim 0.7.3

### How do I get set up? ###

Step 1. If you don't have a workspace, create a [workspace](http://wiki.ros.org/catkin/Tutorials/create_a_workspace).
Step 2. Clone the repository.
```
$ cd ~/catkin_ws/src
$ git clone https://rafael_rodriguesdasilva@bitbucket.org/rafael_rodriguesdasilva/arnlserverros.git
```
Step 3. Compile

```
$ cd ~/catkin_ws
$ catkin_make
```

### How to run tests? ###

Step 1. Open a terminal window and run [roscore](http://wiki.ros.org/roscore).

```
$ roscore
```
Step 2. Open a new terminal and execute MobileSim
```
$  MobileSim

```
Step 3. Open a new terminal and execute arnlServerRos
```
$ source ~/catkin_ws/devel/setup.bash
$ rosrun gotoactionexampleros gotoactionexample_node

```
Step 5. The robot should make a square trajectory.

### Who do I talk to? ###

Rafael Rodrigues da Silva
rrodri17@nd.edu